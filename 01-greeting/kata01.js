function greet (name) {
    let saludo

    if (name === null) {
        name = 'colega'
    }

    if (name === name.toUpperCase()) {
        saludo = 'HOLA, '
    } else {
        saludo = 'Hola, '
    }
    return saludo + name
}

module.exports = { greet }