The Greeting Kata

basado en https://github.com/testdouble/contributing-tests/wiki/Greeting-Kata

Requisito 1

Escribir una función `greet(name)` que interpola el nombre en un saludo. Por ejemplo, para `name` con valor `"Romualdo"` debería devolver 
```
Hola, Romualdo
```

Requisito 2

Gestionar los nulls con un valor por defecto. Por ejemplo, si el `name` es `null` debería devolver
```
Hola, colega
```

Requisito 3

Gestionar si se grita. Si el nombre se pasa en mayúsculas, debería devolver el saludo en mayúsculas. Por ejemplo, para `name` con valor `"ROMUALDO"` debería devolver `"HOLA, ROMUALDO"`
```
HOLA, ROMUALDO
```

Requisito 4

Gestionar varios nombres. Si el parámetro `name` es una lista de nombres separadas por comas, devolver una línea por cada nombre. Por ejemplo, para `name` con valor `"Romualdo, Gertrudis"` debería devolver 
```
Hola, Romualdo
Hola, Gertrudis
```

Requisito 5

Gestionar los gritos para varios nombres. Si uno de los nombres está en mayúsculas, devolver el saludo a esa persona en mayúsculas. Por ejemplo, para `name` con valor `"Romualdo, GERTRUDIS"` debería devolver 
```
Hola, Romualdo
HOLA, GERTRUDIS
```
