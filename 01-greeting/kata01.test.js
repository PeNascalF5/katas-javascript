const { greet } = require('./kata01.js')

test("Requisito 1", function () {
    expect(greet('Romualdo')).toBe('Hola, Romualdo');
    expect(greet('Bernardo')).toBe('Hola, Bernardo');
});

test("Requisito 2", function () {
    expect(greet(null)).toBe('Hola, colega');
});


test("Requisito 3", function () {
    expect(greet('ROMUALDO')).toBe('HOLA, ROMUALDO');
    expect(greet('BERNARDO')).toBe('HOLA, BERNARDO');
});
